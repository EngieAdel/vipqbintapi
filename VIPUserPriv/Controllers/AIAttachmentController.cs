﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VIPQBAPI.Models;

namespace VIPQBAPI.Controllers
{
    public class AIAttachmentController : ApiController
    {

        // POST api/<controller>
        public bool Post([FromBody]QBAa model)
        {
            Logger.log.Info("Posting : AIattachmentsController ");
            Logger.log.Info("filename : " + model.FileName);
            try
            {
                model.getAA();
                if (model.AItem.ResponsibleParty.ToLower() == "partner")
                    return EmailChk.GetMailValidation(model.AItem.Contact_Email);
                else return false;
            }
            catch (System.Exception e)
            {
                Logger.log.Error("Error: for AI: " + model.QbAIAGUID);
                var st = new StackTrace(e, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber();
                Logger.log.Info("Exception occurs in line no.: " + line);
                if (e.InnerException != null) Logger.log.Error("Inner exception: " + e.InnerException);
                Logger.log.Error("Exception occurs: " + e.Message);
                return false;
            }
        }

    }
}