﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VIPQBAPI.Models;

namespace VIPQBAPI.Controllers
{
    public class ActionItemsController : ApiController
    {
     
        // POST api/<controller>
        public bool Post([FromBody]QBAi model)
        {
            try
            {
             Logger.log.Info("Post AI");
                if (model != null)
                {
                    Logger.log.Info("model contais data");
                    Logger.log.Info("markcompleted " + model.MarkCompleted);
                    
                }
                else
                {
                    Logger.log.Error("model is null");
                    return false;
                }
           
                Logger.log.Info("geActionItem");
                model.geActionItem();

                Logger.log.Info("REsPonsible Party " + model?.ResponsibleParty??string.Empty);
                Logger.log.Info("Contact_Email" +   model?.ContactEmail ?? string.Empty);

                //if (model != null && model.ResponsibleParty.ToLower() == "partner")
                //    return EmailChk.GetMailValidation(model?.ContactEmail?? "");
                // else return false;
                return true;
            }
            catch (System.Exception e)
            {
                Logger.log.Error("Error: for AI: "+ model.QBGUID);
                var st = new StackTrace(e, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber();
                Logger.log.Info("Exception occurs in line no.: " + line);
                if (e.InnerException != null) Logger.log.Error("Inner exception: " + e.InnerException);
                Logger.log.Error("Exception occurs: " + e.Message);
                return false;
            }
        }

        // PUT api/<controller>/5
    }
}