﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace VIPQBAPI.Models
{
    public class QBAa
    {
        public long? QBID { get; set; }
        public long ActionID { get; set; }
        public Guid? QBAIGUID { get; set; }
        public long? RelatedQBAI { get; set; }
        public Guid? QbAIAGUID { get; set; }
        public  ActionItem AItem { get; set; }
        public string Description { get; set; }
        public DateTime DateModified { get; set; }
        public string FileName { get; set; }
        public string Name { get; set; }


        public void getAA()
        {
            string configPath = System.Configuration.ConfigurationManager.AppSettings["QBActionAttachements"];
            string qbpath = System.Configuration.ConfigurationManager.AppSettings["qbpath"];
            string QBVipToken = System.Configuration.ConfigurationManager.AppSettings["VIPUserToken"];


            string action = string.Empty;
            try
            {
                using (ebnPartnerPortal_prodEntities Ebn = new ebnPartnerPortal_prodEntities())
                {
                    AItem = QBAc.getActionItem(Ebn, QBAIGUID, RelatedQBAI);

                    if (AItem != null)
                    {
                        ActionID = AItem.ID;
                        #region new
                        if (QBID == null || Ebn.ActionAttachments.Where(c => c.ActionID == ActionID && c.QBID == QBID).Count() == 0)
                        {

                            Logger.log.Info("new action attachement for actionid: " + ActionID);

                            action = "insert";
                            ActionAttachment actionattachements = new ActionAttachment();
                            actionattachements.ActionID = AItem.ID;
                            if (QBID != null)
                            actionattachements.QBID = Convert.ToInt64(QBID);

                            actionattachements.QBCreated = true;
                            actionattachements.Description = Description;
                            actionattachements.DateModified = DateModified;
                            actionattachements.QbAIAGUID = QbAIAGUID;
                            if (FileName != null)
                            {
                                Logger.log.Info("filename "+FileName);
                                FileName = qbpath + FileName;
                                string myString = FileName.ToString();
                                string tobesearched = "https://";
                                int ix = myString.IndexOf(tobesearched);
                                if (ix != -1)
                                {
                                    string code = HttpUtility.UrlDecode(myString.Substring(ix));
                                    FileName = code;
                                }

                                Logger.log.Info("attach.Filename: = " + FileName);
                                string actionfolderpath = Path.Combine(configPath, "Actionitems_" + AItem.ID.ToString());
                                string actionattachpath = Path.Combine(actionfolderpath, "actionattachment");

                                if (!Directory.Exists(actionfolderpath))
                                    Directory.CreateDirectory(actionfolderpath);
                                if (!Directory.Exists(actionattachpath))
                                    Directory.CreateDirectory(actionattachpath);

                                // rec.DownloadFile("Content", actionattachpath, 1);
                                string fNAme = FileName.Substring(FileName.IndexOf("/va/") + 4);


                                if (fNAme.Length > 100)
                                {
                                    int index = fNAme.LastIndexOf(".");
                                    string extension = fNAme.Substring(index);
                                    fNAme = fNAme.Substring(0, 90) + extension;
                                }


                                actionattachements.FileName = fNAme;
                                actionattachements.FilePath = actionattachpath;
                                actionattachements.Name = !string.IsNullOrEmpty(Name) ? Name : fNAme;

                                string fnamepath = Path.Combine(actionattachpath, fNAme);
                                FileName = FileName + "?usertoken=" + QBVipToken;

                                using (WebClient client = new WebClient())
                                {
                                    Logger.log.Info("attach.Filename: = " + FileName);
                                    Logger.log.Info("fnamepath: = " + fnamepath);
                                    client.DownloadFile(FileName, fnamepath);
                                }
                            }
                            Ebn.ActionAttachments.Add(actionattachements);
                            Ebn.SaveChanges();
                            Logger.log.Info("Insert Action attachment  for ActionID: " + AItem.ID);
                        }
                        #endregion
                        #region update
                        else
                        {

                            ActionAttachment updactionattach = null;

                            if (QBID != null)
                                updactionattach = Ebn.ActionAttachments.Where(c => c.ActionID == AItem.ID && c.QBID == QBID).FirstOrDefault();
                            if (updactionattach == null && QbAIAGUID != null)
                                updactionattach = Ebn.ActionAttachments.Where(c => c.ActionID == AItem.ID && c.QbAIAGUID == QbAIAGUID).FirstOrDefault();

                            if (updactionattach != null)
                            {
                                action = "update";
                                Logger.log.Info("update attachement for actionid: " + AItem.ID);

                                updactionattach.Description = Description != null ? Description : Name;
                                updactionattach.QbAIAGUID = QbAIAGUID;

                                string actionfolderpath = Path.Combine(configPath, "Actionitems_" + AItem.ID.ToString());
                                string actionattachpath = Path.Combine(actionfolderpath, "actionattachment");
                                if (!Directory.Exists(actionfolderpath))
                                    Directory.CreateDirectory(actionfolderpath);
                                if (!Directory.Exists(actionattachpath))
                                    Directory.CreateDirectory(actionattachpath);

                                string currentfile = Directory.GetFiles(actionattachpath).Select(Path.GetFileName).FirstOrDefault();
                                if (currentfile != null)
                                {
                                    Logger.log.Info(currentfile);
                                    try
                                    {
                                        string currentfilePath = Directory.GetFiles(actionattachpath).FirstOrDefault();
                                        File.Delete(currentfilePath);
                                    }
                                    catch (Exception e)
                                    {
                                        var st = new StackTrace(e, true);
                                        var frame = st.GetFrame(0);
                                        var line = frame.GetFileLineNumber();
                                        Logger.log.Error("Exception occurs in line: " + line);
                                        if (e.InnerException != null) Logger.log.Error("Error " + e.InnerException);
                                        Logger.log.Error("Error:  deleting oldfile for attach ID: " + updactionattach.ID + e.Message);

                                        SndMail sndemail = new SndMail();

                                        try
                                        {
                                            sndemail.Snd();
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.log.Error("Failed to send email" + ex.Message);
                                        }
                                    }
                                }

                                if (!string.IsNullOrEmpty(FileName))
                                {
                                    FileName = qbpath + FileName;
                                    string myString = FileName.ToString();
                                    string tobesearched = "https://";
                                    int ix = myString.IndexOf(tobesearched);
                                    if (ix != -1)
                                    {
                                        string code = HttpUtility.UrlDecode(myString.Substring(ix));
                                        FileName = code;
                                    }
                                }
                                string fNAme = FileName.Substring(FileName.IndexOf("/va/") + 4);


                                if (fNAme.Length > 100)
                                {
                                    int index = fNAme.LastIndexOf(".");
                                    string extension = fNAme.Substring(index);
                                    fNAme = fNAme.Substring(0, 90) + extension;
                                }

                                updactionattach.FileName = fNAme;
                                updactionattach.FilePath = actionattachpath;
                                updactionattach.Name = !string.IsNullOrEmpty(Name) ? Name : fNAme;


                                string fnamepath = Path.Combine(actionattachpath, fNAme);
                                FileName = FileName + "?usertoken=" + QBVipToken;

                                using (WebClient client = new WebClient())
                                {
                                    client.DownloadFile(FileName, fnamepath);
                                }

                                Ebn.SaveChanges();
                                Logger.log.Info("update Action attachments  for ActionID: " + AItem.ID);

                            }
                            else
                            {
                                Logger.log.Info("No updates Action Attachement  for ActionID: " + AItem.ID);
                            }
                        }
                        #endregion
                    }
                }

            }

            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        var st = new StackTrace(e, true);
                        var frame = st.GetFrame(0);
                        var line = frame.GetFileLineNumber();
                        Logger.log.Error("Exception occurs in line: " + line);
                        Logger.log.Error("Error:can not " + action + " update Action Attachment  for ActionID: " + AItem.ID + " s- Property: " + ve.PropertyName + ", Error:" + ve.ErrorMessage);

                    }
                }
                SndMail sndemail = new SndMail();
                try
                {
                    sndemail.Snd();
                }
                catch (Exception ex)
                {
                    var st = new StackTrace(e, true);
                    var frame = st.GetFrame(0);
                    var line = frame.GetFileLineNumber();
                    Logger.log.Error("Exception occurs in line: " + line);
                    Logger.log.Error("Failed to send email" + ex.Message);
                }
            }
            catch (Exception e)
            {
                
                    Logger.log.Error("Exception occurs: " + e.Message);
                    var st = new StackTrace(e, true);
                    var frame = st.GetFrame(0);
                    var line = frame.GetFileLineNumber();
                    if(e.InnerException != null) Logger.log.Error("Exception occurs in line: " + e.InnerException);
                    Logger.log.Error("Exception occurs in line: " + line);
                    Logger.log.Error("Error: " + action + "  Action Attachment  for ActionID: " + AItem.ID + " Error: " + e.Message);
                

                SndMail sndemail = new SndMail();

                try
                {
                    sndemail.Snd();
                }
                catch (Exception ex)
                {
                    Logger.log.Error("Failed to send email" + ex.Message);
                }
            }
        }
    }
}
        