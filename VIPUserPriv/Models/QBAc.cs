﻿using eBNQuickBaseLibrary;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace VIPQBAPI.Models
{
    public class QBAc
    {
        public string Comment { get; set; }
        public string FileAttachment { get; set; }
        public long ActionID { get; set; }
        public Nullable<long> QBID { get; set; }
        public Nullable<bool> QBCreated { get; set; }
        public string UserName { get; set; }
        public Nullable<System.DateTime> ActionDate { get; set; }
        public Nullable<System.DateTime> DateModified { get; set; }
        public long RelatedQBAI { get; set; }
        public Guid? QBCommentGUID { get; set; }
        public Guid? QBAIGUID { get; set; }
        public ActionItem aItem { get; set; }





        public void getAC()
        {
            string configPath = System.Configuration.ConfigurationManager.AppSettings["QBActionAttachements"];
            string qbpath = System.Configuration.ConfigurationManager.AppSettings["qbpath"];
            string QBVipToken = System.Configuration.ConfigurationManager.AppSettings["VIPUserToken"];

            string action = string.Empty;
            try
            {
                using (ebnPartnerPortal_prodEntities Ebn = new ebnPartnerPortal_prodEntities())
                {
                    Logger.log.Info("QBAIGUID: " + QBAIGUID ?? null);
                    Logger.log.Info("RelatedQBAI: " + RelatedQBAI ?? null);
                    aItem = getActionItem(Ebn, QBAIGUID, RelatedQBAI);
                    if (aItem != null)
                    {
                        ActionID = aItem.ID;
                        Logger.log.Info("found related AI");



                        #region new
                        if (QBID == null || Ebn.ActionComments.Where(c => c.ActionID == ActionID && c.QBID == QBID).Count() == 0)
                        {
                            action = "insert";
                            ActionDate = DateTime.Now;
                            DateModified = DateTime.Now;

                            Logger.log.Info("new action comment for actionGUID: " + QBAIGUID);

                            ActionComment actioncomments = new ActionComment();

                            if (!string.IsNullOrEmpty(Comment))
                                actioncomments.Comment = Comment;
                            else
                                actioncomments.Comment = "N/A";

                            actioncomments.ActionID = ActionID;
                            actioncomments.QBID = QBID;
                            actioncomments.QBCreated = true;
                            actioncomments.UserName = UserName;
                            actioncomments.ActionDate = ActionDate;
                            actioncomments.DateModified = DateModified;
                            actioncomments.QbAcGUID = QBCommentGUID;
                            Ebn.ActionComments.Add(actioncomments);
                            Ebn.SaveChanges();

                            if (FileAttachment != null)
                            {
                                Logger.log.Info("filename " + FileAttachment);
                                FileAttachment = qbpath + FileAttachment;

                                string myString = FileAttachment.ToString();
                                string tobesearched = "https://";
                                int ix = myString.IndexOf(tobesearched);
                                if (ix != -1)
                                {
                                    string code = HttpUtility.UrlDecode(myString.Substring(ix));
                                    FileAttachment = code;
                                }

                                string fNAme = FileAttachment.Substring(FileAttachment.IndexOf("/va/") + 4);

                                if (fNAme.Length > 100)
                                {
                                    int index = fNAme.LastIndexOf(".");
                                    string extension = fNAme.Substring(index);
                                    fNAme = fNAme.Substring(0, 90) + extension;
                                }
                                if (!string.IsNullOrEmpty(fNAme))
                                {
                                    string actionfolderpath = Path.Combine(configPath, "Actionitems_" + aItem.ID.ToString());
                                    string actioncommentspath = Path.Combine(actionfolderpath, "actioncomments");
                                    // long commentID = Ebninsert.ActionComments.Max(c => c.ID);
                                    string curractioncommentspath = Path.Combine(actioncommentspath, "comment_" + actioncomments.ID);// rec["Record ID#"]);

                                    if (!Directory.Exists(actionfolderpath))
                                        Directory.CreateDirectory(actionfolderpath);
                                    if (!Directory.Exists(actioncommentspath))
                                        Directory.CreateDirectory(actioncommentspath);
                                    if (!Directory.Exists(curractioncommentspath))
                                        Directory.CreateDirectory(curractioncommentspath);

                                    actioncomments.FileAttachment = fNAme;
                                    actioncomments.FilePath = curractioncommentspath;

                                    string fnamepath = Path.Combine(curractioncommentspath, fNAme);
                                    FileAttachment = FileAttachment + "?usertoken=" + QBVipToken;

                                    Logger.log.Info(FileAttachment);
                                    using (WebClient client = new WebClient())
                                    {
                                        client.DownloadFile(FileAttachment, fnamepath);
                                    }

                                }
                            }
                            //  Ebninsert.ActionComments.Add(actioncomments);
                            Ebn.SaveChanges();

                            Logger.log.Info("Insert Action Comment  for ActionID: " + ActionID);

                        }

                        #endregion
                        #region update
                        else
                        {
                            action = "update";
                            ActionComment updactioncomments = null;

                            if (QBID != null)
                                updactioncomments = Ebn.ActionComments.Where(c => c.ActionID == ActionID && c.QBID == QBID).FirstOrDefault();
                            if (updactioncomments == null && QBCommentGUID != null)
                                updactioncomments = Ebn.ActionComments.Where(c => c.ActionID == ActionID && c.QbAcGUID == QBCommentGUID).FirstOrDefault();


                            if (updactioncomments != null)
                            {
                                Logger.log.Info("update action comment for actionid: " + ActionID);
                                Logger.log.Info("comment: " + Comment);

                                updactioncomments.Comment = Comment;
                                updactioncomments.QBID = QBID;


                                if (FileAttachment != null)
                                {
                                    Logger.log.Info("filename " + FileAttachment);
                                    FileAttachment = qbpath + FileAttachment;

                                    string myString = FileAttachment.ToString();
                                    string tobesearched = "https://";
                                    int ix = myString.IndexOf(tobesearched);
                                    if (ix != -1)
                                    {
                                        string code = HttpUtility.UrlDecode(myString.Substring(ix));
                                        FileAttachment = code;
                                    }


                                    string fNAme = FileAttachment.Substring(FileAttachment.IndexOf("/va/") + 4);

                                    if (fNAme.Length > 100)
                                    {
                                        int index = fNAme.LastIndexOf(".");
                                        string extension = fNAme.Substring(index);
                                        fNAme = fNAme.Substring(0, 90) + extension;
                                    }
                                    if (!string.IsNullOrEmpty(fNAme))
                                    {
                                        string actionfolderpath = Path.Combine(configPath, "Actionitems_" + ActionID.ToString());
                                        string actioncommentspath = Path.Combine(actionfolderpath, "actioncomments");
                                        updactioncomments.QbAcGUID = QBCommentGUID;

                                        string curractioncommentspath = Path.Combine(actioncommentspath, "comment_" + updactioncomments.ID);

                                        if (!Directory.Exists(actionfolderpath))
                                            Directory.CreateDirectory(actionfolderpath);
                                        if (!Directory.Exists(actioncommentspath))
                                            Directory.CreateDirectory(actioncommentspath);
                                        if (!Directory.Exists(curractioncommentspath))
                                            Directory.CreateDirectory(curractioncommentspath);


                                        /* delete file
                                         * ----------------------------------------------------------------*/
                                        try
                                        {

                                            string fnamepath = Path.Combine(curractioncommentspath, fNAme);

                                            // comments has only 1 file
                                            string currentfile = Directory.GetFiles(curractioncommentspath).Select(Path.GetFileName).FirstOrDefault();
                                            string currentfilePath = Directory.GetFiles(curractioncommentspath).FirstOrDefault();
                                            Logger.log.Info(currentfile); //Copy of DR_RiggsAbney_CommunityCare_20170608.xlsx
                                            Logger.log.Info(FileAttachment);
                                            Logger.log.Info(currentfilePath); //C:\eBNServer\Files\eBNPortalAttachments\QB-attachements\Actionitems_23056\actioncomments\comment_20581\Copy of DR_RiggsAbney_CommunityCare_20170608.xlsx
                                            if (currentfile != null) /// will always be false because of name substring.
                                                File.Delete(currentfilePath);

                                            updactioncomments.FileAttachment = fNAme;
                                            updactioncomments.FilePath = curractioncommentspath;

                                            FileAttachment = FileAttachment + "?usertoken=" + QBVipToken;
                                            Logger.log.Info("FileAttachment: " + FileAttachment);
                                            using (WebClient client = new WebClient())
                                            {
                                                Logger.log.Info("Download Attach: " + fnamepath);
                                                Logger.log.Info(fnamepath);
                                                client.DownloadFile(FileAttachment, fnamepath);
                                            }


                                        }
                                        catch (Exception e)
                                        {
                                            var st = new StackTrace(e, true);
                                            var frame = st.GetFrame(0);
                                            var line = frame.GetFileLineNumber();
                                            Logger.log.Error("Exception occurs in line: " + line);
                                            if (e.InnerException != null) Logger.log.Error("Error " + e.InnerException);
                                            Logger.log.Error("Error:  deleting oldfile for comment ID: " + updactioncomments.ID + e.Message);

                                            SndMail sndemail = new SndMail();

                                            try
                                            {
                                                sndemail.Snd();
                                            }
                                            catch (Exception ex)
                                            {
                                                Logger.log.Error("Failed to send email" + ex.Message);
                                            }
                                        }
                                    }
                                }

                                Ebn.SaveChanges();
                                Logger.log.Info("successfully update Action Comment  for ActionID: " + ActionID);
                            }
                            else
                            {
                                Logger.log.Info("No updates Action Comment  for ActionID: " + ActionID);
                            }
                        }
                        #endregion
                    }
                    else
                        Logger.log.Info("Coud not found related AI for comment: " + QBCommentGUID);


                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        var st = new StackTrace(e, true);
                        var frame = st.GetFrame(0);
                        var line = frame.GetFileLineNumber();
                        Logger.log.Error("Exception occurs in line: " + line);
                        Logger.log.Error("Error:can not " + action + " Action Comment  for ActionID: " + ActionID + " s- Property: " + ve.PropertyName + ", Error:" + ve.ErrorMessage);

                    }
                }
                SndMail sndemail = new SndMail();
                try
                {
                    sndemail.Snd();
                }
                catch (Exception ex)
                {
                    Logger.log.Error("Failed to send email" + ex.Message);
                }
            }
            catch (Exception e)
            {
                Logger.log.Error("Exception occurs: " + e.Message);
                var st = new StackTrace(e, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber();
                Logger.log.Error("Exception occurs in line: " + line);
                if (e.InnerException != null) Logger.log.Error("Error " + e.InnerException);
                Logger.log.Error("Error: " + action + " Action Comment  for ActionID: " + aItem.ID + " Error: " + e.Message);

                SndMail sndemail = new SndMail();

                try
                {
                    sndemail.Snd();
                }
                catch (Exception ex)
                {
                    var exst = new StackTrace(e, true);
                    // Get the top stack frame
                    var exframe = exst.GetFrame(0);
                    // Get the line number from the stack frame t
                    var exline = exframe.GetFileLineNumber();
                    Logger.log.Info(line);
                    Logger.log.Error("Failed to send email" + ex.Message);
                }
            }

        }


        public static ActionItem getActionItem(ebnPartnerPortal_prodEntities Ebn, Guid? QBAIGUID, long? RelatedQBAI)
        {
            try
            {
                ActionItem actionItem;

                if (QBAIGUID != null) // new comments wiz API
                    actionItem = Ebn.ActionItems.Where(c => c.QBGUID == QBAIGUID).FirstOrDefault();
                else // (RelatedQBAI != null )  //old AI wiz old tool
                    actionItem = Ebn.ActionItems.Where(c => c.QBRecordID == RelatedQBAI).FirstOrDefault();

                return actionItem;
            }
            catch (DbEntityValidationException e)
            {
                if (e.InnerException != null)
                    Logger.log.Error("inner exception: " + e.InnerException);

                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        var st = new StackTrace(e, true);
                        var frame = st.GetFrame(0);
                        var line = frame.GetFileLineNumber();
                        Logger.log.Error("Exception occurs in line: " + line);
                        Logger.log.Error("error message: " + ve.ErrorMessage);
                        Logger.log.Error("property: " + ve.PropertyName);
                        Logger.log.Error("failed to get related AI");

                    }
                }
                SndMail sndemail = new SndMail();
                try
                {
                    sndemail.Snd();
                }
                catch (Exception ex)
                {
                    Logger.log.Error("Failed to send email" + ex.Message);
                }
                return null;
            }
            catch (Exception e)
            {

                Logger.log.Error("failed to get related AI");
                var st = new StackTrace(e, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber();
                Logger.log.Error("Exception occurs in line: " + line);
                if (e.InnerException != null) Logger.log.Error("Error " + e.InnerException);

                SndMail sndemail = new SndMail();

                try
                {
                    sndemail.Snd();
                }
                catch (Exception ex)
                {
                    var exst = new StackTrace(e, true);
                    // Get the top stack frame
                    var exframe = exst.GetFrame(0);
                    // Get the line number from the stack frame t
                    var exline = exframe.GetFileLineNumber();
                    Logger.log.Info(exline);
                    Logger.log.Error("Failed to send email" + ex.Message);
                }
                return null;
            }

        }


    }
}