﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace VIPQBAPI.Models
{
    public class QBGS
    {
        public string customerName { get; set; }
        public string carrierName { get; set; }
        public string orderType { get; set; }
        public DateTime? oEStartDate { get; set; }


        public string getGS()
        {
            Logger.log.Info("get GS contact");

            string outputMail = null;
            long myOrderID = 0;
            try
            {
                using (ebnPartnerPortal_prodEntities ebnDB = new ebnPartnerPortal_prodEntities())
                {
                    if (!orderType.ToString().Contains("OE"))
                        myOrderID = ebnDB.CustomerCarriers_View.Where(o => o.CustomerName == customerName && o.CarrierName == carrierName && o.ServiceType == null && o.OrderType == "EDI").Select(o => o.OrderID).FirstOrDefault();
                    else if (orderType.ToString().Contains("OE"))
                        myOrderID = ebnDB.CustomerCarriers_View.Where(o => o.CustomerName == customerName && o.CarrierName == carrierName && (o.ServiceType == 3 || o.ServiceType == 2) && o.OEStartDate == oEStartDate).Select(o => o.OrderID).FirstOrDefault();


                    if (myOrderID != 0)
                    {
                        long? customerId = null;

                        string partnerContactEmail = null;
                        UserProfile up = null;
                        string orderMappingContactEmail = null;

                        #region OMC
                        orderMappingContactEmail = ebnDB.OrderMappingContacts.Where(m => m.OrderId == myOrderID).Select(m => m.ContactEmail).FirstOrDefault() ?? null;

                        if (orderMappingContactEmail != null)
                            outputMail = ebnDB.UserProfiles.Where(o => o.Email == orderMappingContactEmail && o.SendGSMail == true).Select(o => o.Email).FirstOrDefault() ?? null;

                        #endregion
                        #region PartnerContact
                        if (outputMail == null)
                        {
                            customerId = ebnDB.Customers.Where(o => o.OrderID == myOrderID).Select(o => o.CustomerID).FirstOrDefault();
                            partnerContactEmail = ebnDB.CustomerContacts.Where(o => o.CustomerID == customerId && o.IsPartner == true).Select(o => o.CustomerContactEmail).FirstOrDefault() ?? null;

                            if (partnerContactEmail != null)
                            {
                                outputMail = ebnDB.UserProfiles.Where(o => o.Email == partnerContactEmail && o.SendGSMail == true).Select(o => o.Email).FirstOrDefault() ?? null;
                            }
                        }
                        #endregion



                    }

                }
                return outputMail;
            }
            catch (System.Exception e)
            {
                Logger.log.Error("Error getting gs contact");
                var st = new StackTrace(e, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber();
                
                Logger.log.Info("Exception occurs in line no.: " + line);
                if (e.InnerException != null) Logger.log.Error("Inner exception: " + e.InnerException);
                Logger.log.Error("Exception occurs: " + e.Message);
                return null;
            }
        }

    }
}