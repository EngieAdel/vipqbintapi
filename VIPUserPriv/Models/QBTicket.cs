﻿using eBNQuickBaseLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VIPQBAPI.Models
{
    public class QBTicket
    {
        static string Domain = System.Configuration.ConfigurationManager.AppSettings["Domain"];
        static string UName = System.Configuration.ConfigurationManager.AppSettings["UName"];
        static string Pwd = System.Configuration.ConfigurationManager.AppSettings["PWD"];

        public static string Ticket { get; set; }


        public static string QBtickets()
        {
            using (var connection = new QuickBaseConnection(Domain, UName, Pwd))
            {
                if (string.IsNullOrEmpty(connection.Ticket))
                    connection.Connect();

                Ticket = connection.Ticket;


                return Ticket;
            }
        }
    }
}